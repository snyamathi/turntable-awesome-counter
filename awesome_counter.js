/*
 * Copyright (c) 2013, SNyamathi
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *    * Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *    * Neither the name of SNyamathi nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

var awesomeCount;
var cookies;
var metadata = {upvotes:0, downvotes:0};

setInterval(fetchMetadata, 10000);
setInterval(updateAwesomeCount, 100);

function fetchMetadata() {
  // TODO: update this to only fetch the cookies on cookie change
  var cookies = getCookies();

  jQuery.get("http://turntable.fm/api/room.shortcut_info", {
    shortcut: document.location.pathname.substring(1), 
    extended: false,
    userid: cookies['turntableUserId'],
    userauth: cookies['turntableUserAuth'],
    client: 'web',
    decache: new Date().valueOf()/1000>>0
  }).done(function(data) {
    metadata = data[1]['room']['metadata'];
    if (metadata != null) {
      updateAwesomeCount(metadata);
    }
  });
}

function getCookies() {
  var result = {};
  var cookies = document.cookie.split('; ');
  for (var i=0; i<cookies.length; ++i) {
    var cur = cookies[i].split('=');
    result[cur[0]] = cur[1];
  }
  return result;
}

function updateAwesomeCount() {
  awesomeCount = $('#awesomeCount');

  if (awesomeCount[0] == null) {
    var pointDisplay = $('.point-display');
    pointDisplay[0].innerHTML = pointDisplay[0].innerHTML + '<div id="awesomeCount">0</div>';
  }

  $('#awesomeCount')[0].innerHTML = "<span class='emoji emoji-thumbsup'></span>"+metadata.upvotes+"<span class='emoji emoji-thumbsdown'></span>"+metadata.downvotes;
}